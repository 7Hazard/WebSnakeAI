var Shapes;
(function (Shapes) {
    var Rectangle = paper.Rectangle;
    var Path = paper.Path;
    class Shape {
        constructor(x, y) {
            this.x = x;
            this.y = y;
        }
    }
    Shapes.Shape = Shape;
    class Rect extends Shape {
        constructor(x, y, width, height, stroke = "black", fill = "white") {
            super(x, y);
            this.width = width;
            this.height = height;
            this.color = stroke;
            this.path = new Path.Rectangle(new Rectangle(this.x, this.y, this.width, this.height));
            this.path.strokeColor = stroke;
            this.path.fillColor = fill;
        }
    }
    Shapes.Rect = Rect;
})(Shapes || (Shapes = {}));
var Snake;
(function (Snake_1) {
    var Rect = Shapes.Rect;
    var Size = paper.Size;
    var Point = paper.Point;
    window.onload = function () {
        Game.Start("gameCanvas", 17, 17);
        console.log("Loaded WebSnakeAI");
    };
    class Game {
        static Start(canvasID, xlen, ylen, tilesize = 30, spos = new Point(Math.floor(xlen / 2), Math.floor(ylen / 2)), slen = 3, sdir = Direction.up) {
            Game.canvas = document.getElementById(canvasID);
            paper.setup(Game.canvas);
            var size = new Size(xlen * tilesize, ylen * tilesize);
            Game.canvas.width = size.width;
            Game.canvas.height = size.height;
            Game.CenterCanvas();
            Game.tilesize = tilesize;
            for (var x = 0; x < xlen; x++) {
                Game.map[x] = [];
                for (var y = 0; y < ylen; y++) {
                    Game.map[x][y] = new Tile(new Point(x, y));
                }
            }
            Game.snake = new Snake(spos, slen);
            setInterval(Game.OnTick, 200);
        }
        static OnTick() {
            Game.snake.Move();
            paper.view.draw();
        }
        static CenterCanvas() {
        }
        static SetApple(x, y) {
        }
    }
    Game.map = [[]];
    Snake_1.Game = Game;
    class Snake {
        constructor(pos, slen, dir = Direction.up) {
            this.body = [];
            for (var i = 0; i < slen; i++) {
                switch (dir) {
                    case Direction.up:
                        {
                            var np = new Point(pos.x, pos.y + i);
                            this.body[i] = np;
                            Game.map[np.x][np.y] = new SnakeTile(np, dir);
                        }
                        break;
                    case Direction.down:
                        {
                            var np = new Point(pos.x, pos.y - i);
                            this.body[i] = np;
                            Game.map[np.x][np.y] = new SnakeTile(np, dir);
                        }
                        break;
                    case Direction.left:
                        {
                            var np = new Point(pos.x + i, pos.y);
                            this.body[i] = np;
                            Game.map[np.x][np.y] = new SnakeTile(np, dir);
                        }
                        break;
                    case Direction.right:
                        {
                            var np = new Point(pos.x - i, pos.y);
                            this.body[i] = np;
                            Game.map[np.x][np.y] = new SnakeTile(np, dir);
                        }
                        break;
                }
            }
        }
        GetDirection() {
            var p = this.body[0];
            return Game.map[p.x][p.y].direction;
        }
        Move() {
            for (var i = 0; i < this.body.length; i++) {
                var p = this.body[i];
                this.body[i] = Game.map[p.x][p.y].Move();
            }
        }
    }
    class Tile {
        constructor(pos, fill = "white") {
            var size = Game.tilesize;
            this.rect = new Rect(pos.x * size, pos.y * size, size, size, "black", fill);
            this.pos = pos;
        }
        HasApple() {
            return false;
        }
        HasSnake() {
            return false;
        }
        Move() {
        }
    }
    class SnakeTile extends Tile {
        constructor(pos, dir) {
            super(pos, "green");
            this.direction = dir;
        }
        Move() {
            var cp = this.pos;
            switch (this.direction) {
                case Direction.up:
                    {
                        var np = cp.clone();
                        np.y--;
                        if (Game.map[np.x][np.y] == null) {
                            console.log("LOST");
                        }
                        Game.map[np.x][np.y] = new SnakeTile(np, this.direction);
                        Game.map[cp.x][cp.y] = new Tile(cp);
                    }
                    return np;
                case Direction.down:
                    {
                        var np = cp.clone();
                        np.y++;
                        if (Game.map[np.x][np.y] == null) {
                            console.log("LOST");
                        }
                        Game.map[np.x][np.y] = new SnakeTile(np, this.direction);
                        Game.map[cp.x][cp.y] = new Tile(cp);
                    }
                    return np;
                case Direction.left:
                    {
                        var np = cp.clone();
                        np.x--;
                        if (Game.map[np.x][np.y] == null) {
                            console.log("LOST");
                        }
                        Game.map[np.x][np.y] = new SnakeTile(np, this.direction);
                        Game.map[cp.x][cp.y] = new Tile(cp);
                    }
                    return np;
                case Direction.right:
                    {
                        var np = cp.clone();
                        np.x++;
                        if (Game.map[np.x][np.y] == null) {
                            console.log("LOST");
                        }
                        Game.map[np.x][np.y] = new SnakeTile(np, this.direction);
                        Game.map[cp.x][cp.y] = new Tile(cp);
                    }
                    return np;
            }
        }
        HasSnake() {
            return true;
        }
    }
    class Apple extends Tile {
        constructor(pos) {
            super(pos, "red");
        }
        HasApple() {
            return true;
        }
    }
    let Direction;
    (function (Direction) {
        Direction[Direction["up"] = 0] = "up";
        Direction[Direction["down"] = 1] = "down";
        Direction[Direction["left"] = 2] = "left";
        Direction[Direction["right"] = 3] = "right";
    })(Direction || (Direction = {}));
})(Snake || (Snake = {}));
//# sourceMappingURL=Game.js.map