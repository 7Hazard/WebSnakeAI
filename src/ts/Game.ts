/// <reference path="Shapes.ts" />

namespace Snake {
    import Rect = Shapes.Rect;
    import Size = paper.Size;
    import Point = paper.Point;

    window.onload = function():void {
        Game.Start("gameCanvas", 17, 17);
        console.log("Loaded WebSnakeAI");
    }

    export class Game {
        static canvas:HTMLCanvasElement;
        static map:Tile[][] = [[]];
        static tilesize;
        static snake:Snake;

        static Start(
            canvasID:string, 
            xlen, ylen, tilesize = 30,
            spos = new Point(Math.floor(xlen/2), Math.floor(ylen/2)),
            slen = 3, 
            sdir:Direction = Direction.up
        ){
            Game.canvas = document.getElementById(canvasID);
            paper.setup(Game.canvas);
            var size = new Size(xlen*tilesize, ylen*tilesize);
            Game.canvas.width = size.width;
            Game.canvas.height = size.height;
            Game.CenterCanvas();
            
            Game.tilesize = tilesize;
            for(var x = 0; x < xlen; x++){
                Game.map[x] = [];
                for(var y = 0; y < ylen; y++){
                    Game.map[x][y] = new Tile(
                        new Point(x, y)
                    );
                }
            }

            Game.snake = new Snake(spos, slen);


            setInterval(Game.OnTick, 200);
        }

        static OnTick(){
            Game.snake.Move();
            paper.view.draw();
        }

        static CenterCanvas(){
            
        }

        static SetApple(x, y){

        }
    }

    class Snake {
        body:Point[] = [];
        constructor(pos, slen, dir:Direction = Direction.up){
            for(var i = 0; i < slen; i++){
                switch(dir){
                    case Direction.up:{
                        var np = new Point(pos.x, pos.y+i);
                        this.body[i] = np;
                        Game.map[np.x][np.y] = new SnakeTile(
                            np,
                            dir
                        );
                    } break;
                    case Direction.down:{
                        var np = new Point(pos.x, pos.y-i);
                        this.body[i] = np;
                        Game.map[np.x][np.y] = new SnakeTile(
                            np,
                            dir
                        );
                    } break;
                    case Direction.left:{
                        var np = new Point(pos.x+i, pos.y);
                        this.body[i] = np;
                        Game.map[np.x][np.y] = new SnakeTile(
                            np,
                            dir
                        );
                    } break;
                    case Direction.right:{
                        var np = new Point(pos.x-i, pos.y);
                        this.body[i] = np;
                        Game.map[np.x][np.y] = new SnakeTile(
                            np,
                            dir
                        );
                    } break;
                }
            }
        }

        GetDirection():Direction{
            var p = this.body[0];
            return Game.map[p.x][p.y].direction;
        }

        Move(){
            for(var i = 0; i < this.body.length; i++){
                var p = this.body[i];
                this.body[i] = Game.map[p.x][p.y].Move();
            }
        }
    }

    class Tile {
        pos:Point;
        private rect:Rect;
        constructor(pos:Point, fill:string = "white"){
            var size = Game.tilesize;
            this.rect = new Rect(
                pos.x*size, pos.y*size, size, size, "black", fill
            );
            this.pos = pos;
        }

        HasApple(){
            return false;
        }

        HasSnake(){
            return false;
        }

        Move():any{
        }
    }

    class SnakeTile extends Tile {
        direction:Direction;
        constructor(pos:Point, dir:Direction){
            super(pos, "green");
            this.direction = dir;
        }

        Move():Point{
            var cp = this.pos;
            switch(this.direction){
                case Direction.up:{
                    var np = cp.clone(); np.y--;
                    if(Game.map[np.x][np.y] == null){
                        console.log("LOST");
                    }
                    Game.map[np.x][np.y] = new SnakeTile(
                        np, this.direction
                    );
                    Game.map[cp.x][cp.y] = new Tile(
                        cp
                    );
                } return np;
                case Direction.down:{
                    var np = cp.clone(); np.y++;
                    if(Game.map[np.x][np.y] == null){
                        console.log("LOST");
                    }
                    Game.map[np.x][np.y] = new SnakeTile(
                        np, this.direction
                    );
                    Game.map[cp.x][cp.y] = new Tile(
                        cp
                    );
                } return np;
                case Direction.left:{
                    var np = cp.clone(); np.x--;
                    if(Game.map[np.x][np.y] == null){
                        console.log("LOST");
                    }
                    Game.map[np.x][np.y] = new SnakeTile(
                        np, this.direction
                    );
                    Game.map[cp.x][cp.y] = new Tile(
                        cp
                    );
                } return np;
                case Direction.right:{
                    var np = cp.clone(); np.x++;
                    if(Game.map[np.x][np.y] == null){
                        console.log("LOST");
                    }
                    Game.map[np.x][np.y] = new SnakeTile(
                        np, this.direction
                    );
                    Game.map[cp.x][cp.y] = new Tile(
                        cp
                    );
                } return np;
            }
        }

        HasSnake(){
            return true;
        }
    }

    class Apple extends Tile {
        constructor(pos:Point){
            super(pos, "red");
        }

        HasApple(){
            return true;
        }
    }

    enum Direction {
        up,
        down,
        left,
        right
    }
}