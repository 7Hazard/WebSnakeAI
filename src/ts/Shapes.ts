namespace Shapes {
    import Rectangle = paper.Rectangle;
    import Path = paper.Path;

    export class Shape {
        x; y;
        constructor(x, y){
            this.x = x;
            this.y = y;
        }
    }
    
    export class Rect extends Shape {
        width; 
        height; 
        color:string; 
        fill:string;
        path:Path.Rectangle;

        constructor(x, y, width, height, stroke="black", fill="white"){
            super(x, y);
            this.width = width;
            this.height = height;
            this.color = stroke;

            this.path = new Path.Rectangle(
                new Rectangle(
                    this.x, this.y, this.width, this.height
                )
            );
            this.path.strokeColor = stroke;
            this.path.fillColor = fill;
        }
    }
}